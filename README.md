# start minikude
- minikube start --kubernetes-version=v1.22.7 
- eval $(minikube docker-env)

# Navigate to folder counter-app
- cd nginx-app
- docker build . -t nginx:1.1

# Navigate to folder counter-app
- cd counter-app
- docker build . -t flask-api

# retrieve flask-api url
- minikube service flask-service

# retrieve nginx url
- minikube service nginx-service

# Connect to MySQL database by setting up a temporary pod as a mysql-client: 
- kubectl run -it --rm --image=mysql --restart=Never mysql-client -- mysql --host mysql --password=<super-secret-password> 
- CREATE DATABASE toppan;
- USE toppan;
- CREATE TABLE counter(number INT );
- INSERT INTO counter (number) values(0);

# deploy all kubernetes manifest 
kubectl apply  -f kube

# port forward to nginx-service
kubectl port-forward svc/nginx-service 8080:8080

# port forward to backen flask-service, routing from client browser to backend service should be handled with ingress controller but due to time constrain sort out with port forward
kubectl port-forward svc/flask-service 5000:5000

# Add prometheus helm chart repo
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

# Install prometheus helm chart 
helm install prometheus prometheus-community/prometheus

# Access prometheus application
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext
minikube service prometheus-server-ext

# Add grafana helm chart repo
helm repo add grafana https://grafana.github.io/helm-charts

# Install grafana helm chart
helm install grafana grafana/grafana

# Access grafana application
- kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-ext
- minikube service grafana-ext

# Get grafana password 
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
