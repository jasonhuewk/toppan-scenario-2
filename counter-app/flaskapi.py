"""Code for a flask API to Create, Read, Update, Delete users"""
import os
from flask import jsonify, request, Flask
from flaskext.mysql import MySQL
from flask_cors import CORS
import logging
import sys
import json

app = Flask(__name__)
CORS(app)
logging.basicConfig(level=logging.DEBUG)

mysql = MySQL()

# MySQL configurations
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = os.getenv("db_root_password")
app.config["MYSQL_DATABASE_DB"] = os.getenv("db_name")
app.config["MYSQL_DATABASE_HOST"] = "mysql"
app.config["MYSQL_DATABASE_PORT"] = 3306
app.config["MYSQL_DATABASE_PORT"] = 3306
mysql.init_app(app)


@app.route("/")
def index():
    data = {"firstname": os.getenv("first_name")}
    resp = jsonify(data)
    resp.status_code = 200
    return resp

@app.route("/number", methods=["GET"])
def users():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM counter")
        rows = cursor.fetchall()
        cursor.close()
        conn.close()
        resp = jsonify(rows)
        print(f'response data : {resp}', file=sys.stderr)
        sys.stdout.flush()
        resp.status_code = 200
        return resp
    except Exception as exception:
        return jsonify(str(exception))
    
@app.route("/update", methods=["POST"])
def update_number():
    json = request.get_json(force=True)
    print(f'request data : {json}', file=sys.stdout)
    number = json["number"]
    sys.stdout.flush()
    if number :
        # save edits
        sql = "UPDATE counter SET number=%s"
        data = (number)
        try:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute(sql, data)
            conn.commit()
            resp = jsonify("User updated successfully!")
            resp.status_code = 200
            cursor.close()
            conn.close()
            return resp
        except Exception as exception:
            return jsonify(str(exception))
    else:
        return jsonify("Please provide id, name, email and pwd")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
